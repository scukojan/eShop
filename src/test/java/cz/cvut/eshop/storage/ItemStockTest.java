package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ItemStockTest {
    
    @Test
    public void createItemStock() {
        ItemStock itemStock = new ItemStock(null);
        assertEquals(0, itemStock.getCount());
    }

    @Test
    public void increaseItemCount() {
        ItemStock itemStock = new ItemStock(null);
        itemStock.IncreaseItemCount(10);

        assertEquals(10, itemStock.getCount());
    }

    @Test
    public void decreaseItemCount() {
        ItemStock itemStock = new ItemStock(null);
        itemStock.decreaseItemCount(10);

        assertEquals(-10, itemStock.getCount());
    }
    
    @Test
    public void toStringItem() {
        Item apple = new StandardItem(1, "apple", 0.5f, "fruit", 1);
        ItemStock itemStock = new ItemStock(apple);
        itemStock.IncreaseItemCount(3);
        String ts = itemStock.toString();
        assertEquals("STOCK OF ITEM:  Item   ID 1   NAME apple   CATEGORY fruit   PRICE 0.5   LOYALTY POINTS 1    PIECES IN STORAGE: 3", ts);
    }

    @Test
    public void getCount() {
        Item apple = new StandardItem(1, "apple", 0.5f, "fruit", 1);
        ItemStock itemStock = new ItemStock(apple);
        itemStock.IncreaseItemCount(3);
        assertEquals(3, itemStock.getCount());
    }

    @Test
    public void getItem() {
        Item apple = new StandardItem(1, "apple", 0.5f, "fruit", 1);
        ItemStock itemStock = new ItemStock(apple);
        Item item = itemStock.getItem();
        assertEquals(apple, item);

    }

}