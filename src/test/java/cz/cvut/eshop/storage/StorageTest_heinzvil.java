package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.Order;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.*;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * @author Vilém Heinz
 *
 * Test file for Storage.java
 * Testing all methods except
 *      constructors
 *      getStockEntries() - it only passes value
 *      printListOfStoredItems() - no good way to test
 */

public class StorageTest_heinzvil {
    private static Item testItem1;
    private static Item testItem2;
    private static Item testItem3;

    private static String category1;
    private static String category2;
    private static String category3;

    private Storage testStorage;

    @BeforeClass
    public static void beforeAll() {
        category1 = "electronics";
        category2 = "books";
        category3 = "clothes";

        testItem1 = new StandardItem(1, "testItem1", 1.1f,
                category1, 5);
        testItem2 = new StandardItem(2, "testItem2", 2.2f,
                category2, 10);
        testItem3 = new StandardItem(3, "testItem3", -0.1f,
                category3, -1);
    }

    @AfterClass
    public static void afterAll() {
    }

    @Before
    public void setUp(){
        testStorage = new Storage();
    }

    @After
    public void tearDown(){
    }


    @Test
    public void insertItems() {
        //act
        testStorage.insertItems(testItem1, 1);
        int actualCount = testStorage.getItemCount(1);

        //assert
        assertEquals(1, actualCount);
    }

    @Test
    public void insertItems_insertItemZeroTimes() {
        //act
        testStorage.insertItems(testItem1, 0);
        int actualCount = testStorage.getItemCount(1);

        //assert
        assertEquals(0, actualCount);
    }

    @Test
    public void removeItems_emptyStorage() {
        //act
        boolean exceptionThrown = false;
        try {
            testStorage.removeItems(testItem1, 1);
        } catch (NoItemInStorage noItemInStorage) {
            exceptionThrown = true;
        }

        //assert
        assertTrue(exceptionThrown);
    }

    @Test
    public void removeItems_removeZeroTimes() {
        //act
        testStorage.insertItems(testItem1, 1);
        int expectedCount = testStorage.getItemCount(testItem1);
        try {
            testStorage.removeItems(testItem1, 0);
        } catch (NoItemInStorage noItemInStorage) {
            fail("Exception while removing item was thrown but it should not have");
        }

        //assert
        assertEquals(expectedCount, testStorage.getItemCount(testItem1));
    }

    @Test
    public void removeItems_removeMoreTimesThanAvailable() {
        //act
        boolean exceptionThrown = false;
        testStorage.insertItems(testItem1, 2);
        try {
            testStorage.removeItems(testItem1, 3);
        } catch (NoItemInStorage noItemInStorage) {
            exceptionThrown = true;
        }


        //assert
        assertTrue(exceptionThrown);
    }

    @Test
    public void removeItems_successfullRemove() {
        //act
        testStorage.insertItems(testItem1, 1);
        testStorage.insertItems(testItem2, 3);
        testStorage.insertItems(testItem3, 0);
        try {
            testStorage.removeItems(testItem1, 1);
        } catch (NoItemInStorage noItemInStorage) {
            fail("Exception while removing item was thrown but it should not have");
        }

        //assert
        assertEquals(0, testStorage.getItemCount(testItem1));
    }

    @Test
    public void processOrder_empty() {
        //setup
        ArrayList<Item> items = new ArrayList<Item>();
        ShoppingCart cart = new ShoppingCart(items);
        Order order = new Order(cart);

        //act
        boolean processOkay = false;
        try {
            testStorage.processOrder(order);
            processOkay = true;
        } catch (NoItemInStorage noItemInStorage) {
            fail("Exception while processing order was thrown but it should not have");
        }

        //assert
        assertTrue(processOkay);
    }

    @Test
    public void processOrder_notEmpty() {
        //setup
        ArrayList<Item> items = new ArrayList<Item>();
        items.add(testItem1);items.add(testItem2);items.add(testItem3);
        ShoppingCart cart = new ShoppingCart(items);
        Order order = new Order(cart);
        testStorage.insertItems(testItem1, 2);
        testStorage.insertItems(testItem2, 2);
        testStorage.insertItems(testItem3, 2);
        ArrayList<Integer> expectedCount = new ArrayList<Integer>();
        expectedCount.add(1);expectedCount.add(1);expectedCount.add(1);

        //act
        try {
            testStorage.processOrder(order);
        } catch (NoItemInStorage noItemInStorage) {
            fail("Exception while processing order was thrown but it should not have");
        }
        ArrayList<Integer> actualCount = new ArrayList<Integer>();
        actualCount.add(testStorage.getItemCount(testItem1));
        actualCount.add(testStorage.getItemCount(testItem2));
        actualCount.add(testStorage.getItemCount(testItem3));

        //assert
        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void processOrder_orderItemNotInStorage() {
        //setup
        ArrayList<Item> items = new ArrayList<Item>();
        items.add(testItem1);items.add(testItem2);items.add(testItem3);
        ShoppingCart cart = new ShoppingCart(items);
        Order order = new Order(cart);

        //act
        boolean exceptionThrown = false;
        try {
            testStorage.processOrder(order);
        } catch (NoItemInStorage noItemInStorage) {
            exceptionThrown = true;
        }

        //assert
        assertTrue(exceptionThrown);
    }

    @Test
    public void getItemCount_byItemExists() {
        //act
        testStorage.insertItems(testItem1, 1);

        //assert
        assertEquals(1, testStorage.getItemCount(testItem1));
    }

    @Test
    public void getItemCount_byItemNotExists() {
        //act
        testStorage.insertItems(testItem1, 1);

        //assert
        assertEquals(0, testStorage.getItemCount(testItem2));
    }

    @Test
    public void getItemCount_byIdExists() {
        //act
        testStorage.insertItems(testItem1, 1);

        //assert
        assertEquals(1, testStorage.getItemCount(1));
    }

    @Test
    public void getItemCount_byIdNotExists() {
        //act
        testStorage.insertItems(testItem1, 1);

        //assert
        assertEquals(0, testStorage.getItemCount(2));
    }

    @Test
    public void getPriceOfAllItems_empty() {
        //assert
        assertEquals(0, testStorage.getPriceOfWholeStock());
    }

    @Test
    //Renamed function name to what it truly does
    public void getPriceOfAllItems_notEmpty() {
        //act
        testStorage.insertItems(testItem1, 1);
        testStorage.insertItems(testItem2, 2);
        testStorage.insertItems(testItem3, 3);

        //assert
        assertEquals(3, testStorage.getPriceOfWholeStock());
    }

    @Test
    public void getItemsOfCategorySortedByPrice_empty() {
        //setup
        Collection<Item> expectedCol = new ArrayList<Item>();

        //act
        ArrayList<Item> actualCol = (ArrayList<Item>) testStorage.getItemsOfCategorySortedByPrice(category1);

        //assert
        assertEquals(expectedCol, actualCol);
    }

    @Test
    public void getItemsOfCategorySortedByPrice_notEmptyWithMultipleCategories() {
        //setup
        Item itemA = new StandardItem(4, "itemA", 1.f, category1, 2);
        Item itemB = new StandardItem(5, "itemB", 2.f, category1, 2);
        testStorage.insertItems(testItem2, 2);
        testStorage.insertItems(itemA, 2);
        testStorage.insertItems(testItem1, 2);
        testStorage.insertItems(itemB, 2);

        //act
        ArrayList<Item> actualCol = (ArrayList<Item>) testStorage.getItemsOfCategorySortedByPrice(category1);
        Collection<Item> expectedCol = new ArrayList<Item>();
        expectedCol.add(itemA);
        expectedCol.add(testItem1);
        expectedCol.add(itemB);

        //assert
        assertEquals(expectedCol, actualCol);
    }
}
