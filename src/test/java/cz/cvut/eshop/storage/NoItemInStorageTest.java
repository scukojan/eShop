package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;

public class NoItemInStorageTest {

    @Test(expected = NoItemInStorage.class)
    public void noItemInStorageTest() throws NoItemInStorage {
        Storage storage = new Storage();
        Item apple = new StandardItem(1, "apple", 0.5f, "fruit", 1);
        storage.insertItems(apple, 2);

        Item orange = new StandardItem(2, "orange", 0.8f, "fruit", 2);
        storage.removeItems(orange, 1);
    }

}
