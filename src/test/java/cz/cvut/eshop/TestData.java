package cz.cvut.eshop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import cz.cvut.eshop.archive.ItemPurchaseArchiveEntry;
import cz.cvut.eshop.archive.PurchasesArchive;
import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.Order;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;

public class TestData {

	private TestData() throws IllegalAccessException {
		throw new IllegalAccessException("Single instance protection");
	}

	public static StandardItem createStandardItem() {
		return new StandardItem(1, "testItem", 3.5f, "testCategory", 10);
	}

	public static PurchasesArchive createPurchaseArchive(Integer id, ItemPurchaseArchiveEntry orderArchive) {
		HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<Integer, ItemPurchaseArchiveEntry>(Collections.singletonMap(id, orderArchive));
		return new PurchasesArchive(itemArchive, new ArrayList<Order>(1));
	}

	public static Order createOrder(Item... items) {
		ShoppingCart shoppingCart;
		if (items == null) {
			shoppingCart = new ShoppingCart();
		} else {
			ArrayList<Item> cartItems = new ArrayList<Item>(Arrays.asList(items));
			shoppingCart = new ShoppingCart(cartItems);
		}
		return new Order(shoppingCart);
	}
}